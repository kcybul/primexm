define(['text!./home-page.html', 'dialoger', 'knockout', 'modaler', 'jquery', 'configs'],
    function(template, dialoger, ko, modaler, $, configs) {
        'use strict';

        //VM
        var ViewModel = function(settings, componentInfo) {
            var self = this;

            self.columns = ko.observableArray();
            self.rows = ko.observableArray();

            self.rowId = ko.observable(0);
            self.rowDataToUpdate = ko.observable('{ "id": 7, "data": ["WINWIN", "true", 7777, "true", 7777, 7.8] }');

            self.setColumns = function(columns) {
                self.columns = ko.observableArray(columns);
            }

            self.setRows = function(rows) {
                self.rows = ko.observableArray(rows);
            }

            self.updateRow = function(rowId, object) {
                for (var i = 0; i < self.rows().length; i++) {
                    if (self.rows()[i].id === parseInt(rowId)) {
                        self.rows()[i] = JSON.parse(object.toString());
                    }
                }

                self.rows.valueHasMutated();
            }

            self.removeRowById = function (rowId) {

                var elemnt = null;

                for (var i = 0; i < self.rows().length; i++) {
                    if (self.rows()[i].id === parseInt(rowId)) {
                        elemnt = self.rows()[i];
                    }
                }

                self.rows.remove(elemnt);
                self.rows.valueHasMutated();
            }

            var init = function() {
                self.setColumns([
                    { id: 1, name: "Symbol" },
                    { id: 2, name: "Bid LP" },
                    { id: 3, name: "Bid" },
                    { id: 4, name: "Ask LP" },
                    { id: 5, name: "Ask" },
                    { id: 6, name: "Spread" }
                ]);

                self.setRows([
                    { id: 1, data: ['AUDCAD', 'true', '0.99206', 'true', '0.99207', '0.1'] },
                    { id: 2, data: ['AUDCHF', 'false', '0.91370', 'true', '0.91371', '0.1'] },
                    { id: 3, data: ['AUDJPY', 'true', '96.212', 'true', '96.215', '0.3'] },
                    { id: 4, data: ['AUDUSD', 'true', '0.96741', 'true', '0.96744', '0.3'] },
                    { id: 5, data: ['CADCHF', 'true', '0.92070', 'true', '0.92071', '0.1'] },
                    { id: 6, data: ['CADJPY', 'true', '96.827', 'true', '96.828', '0.1'] },
                    { id: 7, data: ['CHFJPY', 'true', '105.112', 'true', '105.115', '0.3'] }
                ]);
            }

            init();
        };

        //For tests
        ViewModel.prototype.doSomething = function() {
            this.message('You invoked doSomething() on the viewmodel.');
        };

        return {
            viewModel: {
                createViewModel: function(params, componentInfo) {
                    return new ViewModel(params, componentInfo);
                }
            },
            template: template
        };
    });
