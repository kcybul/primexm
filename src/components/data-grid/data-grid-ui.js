define(['text!./data-grid.html','data-grid', 'router'],
    function(template, dataGrid, router) {
        'use strict';

        function DataGridViewModel(params, componentInfo) {
            var self = this;
            self.columns = params.columns;
            self.rows = params.rows;
        }

        DataGridViewModel.prototype.isPageActive = function (menu) {
            var context = router.context();

            if (context) {
                return menu.url.toLowerCase() === context.route.url.toLowerCase();
            }

            return false;
        };

        return {
            viewModel: {
                createViewModel: function (params, componentInfo) {
                    return new DataGridViewModel(params, componentInfo);
                }
            },
            template: template
        };
    });