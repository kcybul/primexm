Before using this generator, you need a number of tool

1. Install nodejs
2. Install npmjs: npm install -g npm to upgrade npm to at least 2.x.x (type npm -v to get your current version)
    ** If you have problem upgrading your npm version, it may have to do with how Windows handle PATH environment variable order.
3. Install bower: npm install -g bower
4. Install gulpjs: npm install -g gulp

How to run project:

1. Open console and go to project folder.
2. Install npm packages: npm install
3. Install bower packages: bower install
2. Serve web site by: gulp --open